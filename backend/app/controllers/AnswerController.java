package controllers;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import akka.actor.ActorSystem;
import models.PlayerVotes;
import models.SubmitAnswerModel;
import models.SubmitAnswerResponse;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import repositories.AnswerRepository;
import repositories.GameRepository;

public class AnswerController extends Controller {
	
	AnswerRepository _repo;
	ActorSystem _actorSystem;
	
	@Inject
	public AnswerController(ActorSystem actorSystem) {
		_actorSystem = actorSystem;
		_repo = new AnswerRepository();
	}
	
	public Result submitAnswer(Http.Request request) {
		JsonNode json = request.body().asJson();
		SubmitAnswerModel model = Json.fromJson(json, SubmitAnswerModel.class);
		
		boolean lastToHandIn = false;
		
		try {
			GameRepository gameRepo = new GameRepository();
			int round = gameRepo.getGame(model.gameName).currentRound;
			_repo.submitAnswer(model, round);
			int playerCount = gameRepo.getPlayerCount(model.gameName);
			int answerCount = _repo.answerCount(model.gameName, round);
			
			if (answerCount == playerCount) {
				lastToHandIn = true;
				EventSourceController.notifyAllPlayers(model.gameName, "round-completed", null, gameRepo, _actorSystem, model.submitterId);
			}
			
			return ok(Json.toJson(new SubmitAnswerResponse(lastToHandIn)));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
				
	}
	
	public Result getRoundAnswers(String gameName) {
		
		try {
			GameRepository gameRepo = new GameRepository();
			int round = gameRepo.getGame(gameName).currentRound;
			List<PlayerVotes> playersWithVotes = _repo.getPlayersWithVotes(gameName, round);
			
			return ok(Json.toJson(playersWithVotes));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	}
	
}
