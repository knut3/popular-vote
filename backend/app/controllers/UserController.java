package controllers;



import java.util.UUID;

import play.mvc.*;


public class UserController extends Controller {

	
	public UserController() {
	}
	
	public Result getNewUserId() {
		
		UUID userId = UUID.randomUUID();
		return ok(userId.toString());
	}
	


}
