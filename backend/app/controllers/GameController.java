package controllers;



import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import models.AvailablilityModel;
import models.StatementModel;
import models.SubmitAnswerModel;
import models.ClientMessage;
import models.CreateGameModel;
import models.GameTypes;
import models.JoinGameModel;
import models.UserModel;
import play.api.libs.concurrent.Akka;
import play.libs.Json;
import play.mvc.*;
import repositories.StatementRepository;
import repositories.GameRepository;
import services.StatementService;


public class GameController extends Controller {

	GameRepository _repo;
	ActorSystem _actorSystem;

	@Inject
	public GameController(ActorSystem actorSystem) {
		_repo = new GameRepository();
		_actorSystem = actorSystem;
	}
	
	public Result isGameNameAvailable(String gameName) {
		
		boolean isAvailable;
		try {
			isAvailable = _repo.isGameNameAvailable(gameName);
		} catch (Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
		JsonNode result = Json.toJson(new AvailablilityModel(isAvailable));
		
		return ok(result);
		
	}
	
	public Result isPlayerNameAvailable(String gameName, String playerName) {
		boolean isAvailable;
		try {
			isAvailable = _repo.isPlayerNameAvailable(gameName, playerName);
		} catch (Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
		JsonNode result = Json.toJson(new AvailablilityModel(isAvailable));
		
		return ok(result);
	}
	
	public Result createGame(Http.Request request) {
		JsonNode json = request.body().asJson();
		CreateGameModel model = Json.fromJson(json, CreateGameModel.class);
		
		try {
			UUID.fromString(model.creatorId);
		}
		catch(IllegalArgumentException e) {
			return badRequest("Invalid UUID");
		}
		
		if (model.gameName.isEmpty() || model.creatorName.isEmpty()) {
			return badRequest("Empty required field(s)");
		}
		
		if(	!model.gametype.equals(GameTypes.IncludeExplicitContent) &&
			!model.gametype.equals(GameTypes.NoExplicitContent) &&
			!model.gametype.equals(GameTypes.OnlyExplicitContent)) {
			return badRequest("Invalid gametype");
		}
		
		try {
			_repo.createGame(model);
			return ok();
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
	}
	
	public Result joinGame(Http.Request request) {
		JsonNode json = request.body().asJson();
		JoinGameModel model = Json.fromJson(json, JoinGameModel.class);
		
		try {
			_repo.joinGame(model);
			
			EventSourceController.notifyAllPlayers(
					model.gameName, "player-joined", new UserModel(model.userId, model.username), 
					_repo, _actorSystem, model.userId);
			
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
		return ok();
		
	}
	
	public Result getPlayers(String gameName) {
		
		try {
			List<UserModel> users = _repo.getPlayers(gameName);
			return ok(Json.toJson(users));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
	}
	
	public Result isCreator(String gameName, String userId) {
	
		try {
			boolean isCreator = _repo.isCreator(gameName, userId);
			AvailablilityModel result = new AvailablilityModel(isCreator);
			return ok(Json.toJson(result));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
	}
	
	public Result startNewRound(String gameName) {
		
		try {
			String contentType = _repo.getContentType(gameName);
			StatementService statementService = new StatementService();
			statementService.prepareUnusedRandomStatement(gameName, contentType, _repo);
			_repo.incrementCurrentRound(gameName);
			EventSourceController.notifyAllPlayers(gameName, "new-round-starting", null, _repo, _actorSystem);
			return ok();
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
	}
	
	public Result prepareNewStatement(String gameName) {
		try {
			String contentType = _repo.getContentType(gameName);
			StatementService statementService = new StatementService();
			statementService.prepareUnusedRandomStatement(gameName, contentType, _repo);
			
			// Notify players
			EventSourceController.notifyAllPlayers(gameName, "new-statement-ready", null, _repo, _actorSystem);
					
			return ok();
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	}
	
	public Result getCurrentStatement(String gameName) {
		try {
			int id = _repo.getGame(gameName).currentStatementId;
			StatementRepository statementRepo = new StatementRepository();
			StatementModel statement = statementRepo.getStatement(id);
			return ok(Json.toJson(statement));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	}
	
	public Result getCurrentStatementComment(String gameName) {
		try {
			int id = _repo.getGame(gameName).currentStatementId;
			StatementRepository statementRepo = new StatementRepository();
			String statementComment = statementRepo.getStatementComment(id);
			StatementModel model = new StatementModel(id, statementComment);
			return ok(Json.toJson(model));
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	}
	
	


}
