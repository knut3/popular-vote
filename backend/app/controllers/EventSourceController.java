package controllers;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.inject.Inject;

import repositories.GameRepository;
import actors.ClientConnection;
import akka.NotUsed;
import akka.actor.ActorIdentity;
import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.actor.Identify;
import akka.actor.Props;
import akka.japi.Pair;
import akka.pattern.AskableActorSelection;
import akka.stream.Materializer;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueue;
import akka.util.Timeout;
import models.ClientMessage;
import models.UserModel;
import play.libs.EventSource;
import play.mvc.*;
import scala.concurrent.Await;
import scala.concurrent.Future;


public class EventSourceController extends Controller {
	
	private Materializer _materializer;
	private ActorSystem _actorSystem;
	
	@Inject
	public EventSourceController(Materializer materializer, ActorSystem actorSystem) {
		_materializer = materializer;
		_actorSystem = actorSystem;
	}
	
	
	public Result listen(String userId) {

		ActorSelection existingActor =  _actorSystem.actorSelection("akka://application/user/" + userId);
		
		Timeout t = new Timeout(5, TimeUnit.SECONDS);
	    AskableActorSelection asker = new AskableActorSelection(existingActor);
	    Future<Object> fut = asker.ask(new Identify(1), t);
	    ActorIdentity ident;
		try {
			ident = (ActorIdentity)Await.result(fut, t.duration());
		} catch (Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	    Optional<ActorRef> ref = ident.getActorRef();
	    if (ref.isPresent()) {
	    	_actorSystem.stop(ref.get());
	    	 while (!ref.get().isTerminated()) {
	    		 System.out.println("Actor terminating...");
	    		 try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					
				}
	    	 }
	    }
	    
	    @SuppressWarnings("deprecation")
		Source<EventSource.Event, ActorRef> actorRefPoweredEventSource =
				  Source.actorRef(100, OverflowStrategy.dropHead());
		
		Pair<ActorRef, Source<EventSource.Event, NotUsed>> actorRefEventSourcePair =
				  actorRefPoweredEventSource.preMaterialize(_materializer);
		ActorRef eventSourceActor = actorRefEventSourcePair.first();
		Source<EventSource.Event, ?> eventSource = actorRefEventSourcePair.second();
		
		
		_actorSystem.actorOf(
			    ClientConnection.getProps(
			      userId, eventSourceActor
			    ),
			    userId
			);
		
		return ok().chunked(eventSource.via(EventSource.flow())).as(Http.MimeTypes.EVENT_STREAM);

	}
	
	public static void notifyAllPlayers(String gameName, String eventName, Object data, GameRepository repo, ActorSystem actorSystem, String excludeUserId) throws Exception{
		List<UserModel> players = repo.getPlayers(gameName);
		
		for(UserModel player : players) {
			
			if(excludeUserId != null && player.id.equals(excludeUserId)) {
				continue;
			}
			
			ActorSelection playerActor =  actorSystem.actorSelection("akka://application/user/" + player.id);
			playerActor.tell(new ClientMessage(eventName, data), ActorRef.noSender());
			
		}
    }
	
	public static void notifyAllPlayers(String gameName, String eventName, Object data, GameRepository repo, ActorSystem actorSystem) throws Exception{
		notifyAllPlayers(gameName, eventName, data, repo, actorSystem, null);
	
	}

		
}
