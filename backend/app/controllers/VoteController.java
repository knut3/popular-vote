package controllers;



import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import models.AvailablilityModel;
import models.StatementModel;
import models.ClientMessage;
import models.CreateGameModel;
import models.GameTypes;
import models.JoinGameModel;
import models.UserModel;
import models.VoteFinishedModel;
import models.VoteModel;
import play.api.libs.concurrent.Akka;
import play.libs.Json;
import play.mvc.*;
import repositories.StatementRepository;
import repositories.GameRepository;
import repositories.VoteRepository;
import services.StatementService;


public class VoteController extends Controller {

	VoteRepository _repo;
	ActorSystem _actorSystem;

	@Inject
	public VoteController(ActorSystem actorSystem) {
		_repo = new VoteRepository();
		_actorSystem = actorSystem;
	}
	
	public Result startSkipVote(String gameName, String userId) {
		try {
			_repo.addVote(gameName, userId, true);
			GameRepository gameRepo = new GameRepository();
			int numPlayers = gameRepo.getPlayerCount(gameName);
			int neededVotes = numPlayers / 2 + 1;
			VoteModel voteModel = new VoteModel(1, 1, neededVotes);
			EventSourceController.notifyAllPlayers(gameName, "skip-vote-started", voteModel, gameRepo, _actorSystem);
			return ok();
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
	}
	
	public Result submitVote(String gameName, String userId, boolean votedYes) {
		
		try {
			_repo.addVote(gameName, userId, votedYes);
			GameRepository gameRepo = new GameRepository();
			int numPlayers = gameRepo.getPlayerCount(gameName);
			int neededVotes = numPlayers / 2 + 1;
			int yesVotes = _repo.getVotedYesCount(gameName);
			int votesTotal = _repo.getVoteCount(gameName);
			
			boolean votePassed = yesVotes >= neededVotes;
			boolean voteFinished = (votesTotal == numPlayers) || votePassed ;
			
			if (voteFinished) {
				if (votePassed) {
					// prepare new claim
					String contentType = gameRepo.getContentType(gameName);
					StatementService statementService = new StatementService();
					statementService.prepareUnusedRandomStatement(gameName, contentType, gameRepo);
				}
				
				// delete votes
				_repo.deleteVotes(gameName);
				
				// send vote finished event
				VoteFinishedModel model = new VoteFinishedModel(votePassed);
				EventSourceController.notifyAllPlayers(gameName, "vote-finished", model, gameRepo, _actorSystem);
			}
			else {
				VoteModel voteModel = new VoteModel(votesTotal, yesVotes, neededVotes);
				EventSourceController.notifyAllPlayers(gameName, "new-skip-vote-in", voteModel, gameRepo, _actorSystem);				
			}
			
			return ok();
		}
		catch(Exception e) {
			System.out.println(e);
			return internalServerError();
		}
		
	}


}
