package models;

public class SubmitAnswerResponse {
	
	public SubmitAnswerResponse(boolean lastToHandIn) {
		this.lastToHandIn = lastToHandIn;
	}
	
	public boolean lastToHandIn;
}
