package models;

public class VoteModel {
	public int yesVotes;
	public int votesTotal;
	public int yesVotesNeeded;
	
	public VoteModel(int votesTotal, int yesVotes , int yesVotesNeeded) {
		this.votesTotal = votesTotal;
		this.yesVotes = yesVotes;
		this.yesVotesNeeded = yesVotesNeeded;
	}
}
