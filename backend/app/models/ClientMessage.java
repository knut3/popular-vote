package models;

import java.util.UUID;

public class ClientMessage {
	public UUID id;
	public String event;
	public Object data;
	
	public ClientMessage(String event, Object data) {
		this.id = UUID.randomUUID();
		this.event = event;
		this.data = data;
	}
}
