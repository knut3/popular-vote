package models;

public class VoteFinishedModel {
	public boolean votePassed;
	
	public VoteFinishedModel(boolean votePassed) {
		this.votePassed = votePassed;
	}
}
