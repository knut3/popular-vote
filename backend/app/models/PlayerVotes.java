package models;

public class PlayerVotes {
	
	public String name;
	public int votes;
	
	public PlayerVotes(String name, int votes) {
		this.name = name;
		this.votes = votes;
	}

}
