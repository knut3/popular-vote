package models;

public class GameTypes {
	public static final String NoExplicitContent = "no-explicit";
	public static final String IncludeExplicitContent = "include-explicit";
	public static final String OnlyExplicitContent = "only-explicit";
	
	public static short AsShort(String gametype) {
		switch(gametype) {
			case NoExplicitContent : return 1;
			case IncludeExplicitContent : return 2;
			case OnlyExplicitContent : return 3;
			default : throw new IllegalArgumentException(gametype + " is not a valid gametype");
		}
	}
	
	public static String AsString(short gametype) {
		switch(gametype) {
			case 1 : return NoExplicitContent;
			case 2 : return IncludeExplicitContent;
			case 3 : return OnlyExplicitContent;
			default : throw new IllegalArgumentException(gametype + " is not a valid gametype");
		}
	}
}
