package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import models.DatabaseConnectionInfo;

public class VoteRepository {
Connection _con;
	
	public VoteRepository() {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			_con=DriverManager.getConnection(  
			"jdbc:mysql://"+ DatabaseConnectionInfo.Address + "/" + DatabaseConnectionInfo.Name, DatabaseConnectionInfo.Username, DatabaseConnectionInfo.Password);  
		}
		catch(Exception e){ 
			System.out.println(e);
		}  
			 
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();		
		_con.close();			
	}
	
	public void addVote(String gameName, String userId, boolean votedYes) throws Exception{
		Statement statement = _con.createStatement();
		statement.executeUpdate(
				String.format("INSERT INTO votes VALUES ( '%s', '%s', %b)", 
						gameName, userId, votedYes)
				);
	}
	
	public int getVoteCount(String gameName) throws Exception{
		Statement statement = _con.createStatement();
		ResultSet rs = statement.executeQuery(
				String.format("SELECT COUNT(userId) AS NumberOfVotes FROM votes WHERE gameName = '%s'", 
						gameName)
				);
		rs.next();
		return rs.getInt("NumberOfVotes");
	}
	
	public int getVotedYesCount(String gameName) throws Exception{
		Statement statement = _con.createStatement();
		ResultSet rs = statement.executeQuery(
				String.format("SELECT COUNT(userId) AS NumberOfVotes FROM votes WHERE gameName = '%s' AND votedYes = TRUE", 
						gameName)
				);
		rs.next();
		return rs.getInt("NumberOfVotes");
	}
	
	public void deleteVotes(String gameName) throws Exception{
		Statement statement = _con.createStatement();
		statement.executeUpdate(
				String.format("DELETE FROM votes WHERE gameName = '%s'", 
						gameName)
				);
	}
}
