package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.DatabaseConnectionInfo;
import models.PlayerVotes;
import models.SubmitAnswerModel;

public class AnswerRepository {

	Connection _con;
	
	public AnswerRepository() {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			_con=DriverManager.getConnection(  
			"jdbc:mysql://"+ DatabaseConnectionInfo.Address + "/" + DatabaseConnectionInfo.Name, DatabaseConnectionInfo.Username, DatabaseConnectionInfo.Password);  
		}
		catch(Exception e){ 
			System.out.println(e);
		}  
			 
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();		
		_con.close();			
	}
	
	public void submitAnswer(SubmitAnswerModel model, int round) throws Exception{
		
		Statement statement = _con.createStatement();
		statement.executeUpdate(String.format("INSERT INTO answers VALUES ('%s', %d, '%s', '%s')", 
				model.gameName, round, model.submitterId, model.answerUserId));
		
	}
	
	public int answerCount(String gameName, int round) throws Exception{
		Statement statement = _con.createStatement();
		ResultSet rs = statement.executeQuery(String.format(
				"SELECT COUNT(*) AS AnswerCount FROM answers WHERE gameName = '%s' AND round = %d", gameName, round));
		
		rs.next();
		return rs.getInt("AnswerCount");
	}
	
	public List<PlayerVotes> getPlayersWithVotes(String gameName, int round) throws Exception{
		
		String select = "SELECT u.username, COUNT(a.answerUserId) AS Votes";
		String from = "FROM answers a";
		String join = "RIGHT JOIN users_in_games u";
		String on = String.format("ON a.gameName = u.gameName AND a.answerUserId = u.userId AND a.round = %d", round);
		String where = String.format("WHERE u.gameName = '%s'", gameName);
		String groupBy = "GROUP BY a.answerUserId";
		String orderBy = "ORDER BY Votes DESC";
		
		Statement statement = _con.createStatement();
		ResultSet rs = statement.executeQuery(String.format(
				"%s %s %s %s %s %s %s", 
				select, from, join, on, where, groupBy, orderBy));
		
		List<PlayerVotes> playersWithVotes = new ArrayList<PlayerVotes>();
		
		while(rs.next()) {
			
			String playerName = rs.getString("username");
			int votes = rs.getInt("Votes");
			
			playersWithVotes.add(new PlayerVotes(playerName, votes));
		}
		
		return playersWithVotes;
		
	}
	
}
