package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.CreateGameModel;
import models.DatabaseConnectionInfo;
import models.GameModel;
import models.GameTypes;
import models.JoinGameModel;
import models.UserModel;

public class GameRepository {

	Connection _con;
	
	public GameRepository() {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			_con=DriverManager.getConnection(  
			"jdbc:mysql://"+ DatabaseConnectionInfo.Address + "/" + DatabaseConnectionInfo.Name, DatabaseConnectionInfo.Username, DatabaseConnectionInfo.Password);  
		}
		catch(Exception e){ 
			System.out.println(e);
		}  
			 
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();		
		_con.close();			
	}
	
	public boolean isGameNameAvailable(String gameName) throws Exception{
		
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM active_games WHERE name = '" + gameName +"'");
		if(rs.next()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public boolean isPlayerNameAvailable(String gameName, String playerName) throws Exception{
		
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery(
				String.format("SELECT * FROM users_in_games WHERE gameName = '%s' AND username = '%s'",
						gameName, playerName));
		if(rs.next()) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public void createGame(CreateGameModel model) throws Exception {
		Statement insertInActiveGames = _con.createStatement();
		insertInActiveGames.executeUpdate(
				String.format("INSERT INTO active_games VALUES ( '%s', %d, '%s', NULL, NULL )", 
						model.gameName, GameTypes.AsShort(model.gametype), model.creatorId)
				);
		
		Statement insertInUsersAndGames = _con.createStatement();
		insertInUsersAndGames.executeUpdate(
				String.format("INSERT INTO users_in_games VALUES ( '%s', '%s', '%s' )", 
						model.creatorId, model.gameName, model.creatorName)
				);
	}
	
	public List<UserModel> getPlayers(String gameName) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM users_in_games WHERE gameName = '" + gameName +"'");
		
		List<UserModel> users = new ArrayList<UserModel>();
		while(rs.next()) {
			String id = rs.getString("userId");
			String name = rs.getString("username");
			UserModel user = new UserModel(id, name);
			users.add(user);
		}
		
		return users;
		
	}
	
	public boolean isCreator(String gameName, String userId) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery(String.format("SELECT * FROM active_games WHERE name = '%s' AND creatorId = '%s'",
				gameName, userId));
		if(rs.next()) {
			return true;
		}
		else {
			return false;
		}
	}

	public void joinGame(JoinGameModel model) throws Exception{
		
		Statement insertInUsersAndGames = _con.createStatement();
		insertInUsersAndGames.executeUpdate(
				String.format("INSERT INTO users_in_games VALUES ( '%s', '%s', '%s' )", 
						model.userId, model.gameName, model.username)
				);
		
	}
	
	public void updateCurrentStatement(String gameName, int statementId) throws Exception{
		Statement statement = _con.createStatement();
		statement.executeUpdate(
				String.format("UPDATE active_games SET currentStatementId = %d WHERE name = '%s'", 
						statementId, gameName)
				);
	}
	
	public void incrementCurrentRound(String gameName) throws Exception{
		GameModel game = getGame(gameName);
		int newRound = game.currentRound == null ? 1 : game.currentRound + 1;
		Statement statement = _con.createStatement();
		statement.executeUpdate(
				String.format("UPDATE active_games SET currentRound = %d WHERE name = '%s'", 
						newRound, gameName)
				);
	}
	
	public GameModel getGame(String gameName) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM active_games WHERE name = '" + gameName +"'");
		if(rs.next()) {
			
			GameModel model = new GameModel();
			model.name = gameName;
			model.contentType = rs.getShort("contentType");
			model.creatorId = rs.getString("creatorId");
			model.currentStatementId = rs.getInt("currentStatementId");
			model.currentRound = rs.getInt("currentRound");
			return model;
		}
		else {
			throw new Exception(gameName + " does not exist in active games");
		}
	}
	
	public String getContentType(String gameName) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT contentType FROM active_games WHERE name = '" + gameName +"'");
		if(rs.next()) {
			short contentType = rs.getShort("contentType");
			return GameTypes.AsString(contentType);
		}
		else {
			throw new Exception(gameName + " does not exist in active games");
		}
	}
	
	public int getPlayerCount(String gameName) throws Exception{
		
		Statement statement = _con.createStatement();
		ResultSet rs = statement.executeQuery(String.format(
				"SELECT COUNT(userId) AS NumberOfPlayers FROM users_in_games WHERE gameName = '%s'", gameName));
		rs.next();
		return rs.getInt("NumberOfPlayers");
		
	}
	
}
