package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.StatementModel;
import models.DatabaseConnectionInfo;
import models.GameTypes;

public class StatementRepository {

	Connection _con;
	
	public StatementRepository() {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			_con=DriverManager.getConnection(  
			"jdbc:mysql://"+ DatabaseConnectionInfo.Address + "/" + DatabaseConnectionInfo.Name, DatabaseConnectionInfo.Username, DatabaseConnectionInfo.Password);  
		}
		catch(Exception e){ 
			System.out.println(e);
		}  
			 
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		_con.close();
	}
	
	public StatementModel getRandomStatement(String gametype) throws Exception{
		String whereClause = "";
		switch(gametype) {
			case GameTypes.NoExplicitContent:
				whereClause = " WHERE isExplicit = false";
				break;
			case GameTypes.OnlyExplicitContent:
				whereClause = " WHERE isExplicit = true";
				break;
		}
		
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery("SELECT * FROM statements" + whereClause + " ORDER BY rand() LIMIT 1");
		if(!rs.next()) {
			throw new Exception("No claims in the db");
		}
		int id = rs.getInt("id");
		String value = rs.getString("value");
		StatementModel statementModel = new StatementModel(id, value);
		
		return statementModel;
	}
	
	public StatementModel getStatement(int id) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery(String.format("SELECT * FROM statements WHERE id = '%d'", id));
		if(!rs.next()) {
			throw new Exception("No claims with id=" + id + " in the db");
		}
		String value = rs.getString("value");
		StatementModel statementModel = new StatementModel(id, value);
		
		return statementModel;
	}
	
	public void addUsedStatement(String gameName, int statementId) throws Exception{
		Statement stmt = _con.createStatement();
		stmt.executeUpdate(String.format("INSERT INTO used_statements VALUES ('%s', %d)", gameName, statementId));
	}
	
	public boolean isStatementAlreadyUsed(String gameName, int statementId) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs = stmt.executeQuery(String.format(
				"SELECT * FROM used_statements WHERE gameName = '%s' AND statementId = %d", gameName, statementId));
		
		if (rs.next()) {
			return true;
		}
		else {
			return false;
		}
	}

	public String getStatementComment(int id) throws Exception{
		Statement stmt = _con.createStatement();
		ResultSet rs=stmt.executeQuery(String.format("SELECT * FROM statements WHERE id = '%d'", id));
		if(!rs.next()) {
			throw new Exception("No claims with id=" + id + " in the db");
		}
		return rs.getString("comment");
		
	}
	
}
