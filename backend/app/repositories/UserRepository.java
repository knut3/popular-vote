package repositories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import models.DatabaseConnectionInfo;

public class UserRepository {

	Connection _con;
	
	public UserRepository() {
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			_con=DriverManager.getConnection(  
			"jdbc:mysql://"+ DatabaseConnectionInfo.Address + "/" + DatabaseConnectionInfo.Name, DatabaseConnectionInfo.Username, DatabaseConnectionInfo.Password);  
		}
		catch(Exception e){ 
			System.out.println(e);
		}  
			 
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();		
		_con.close();			
	}
	
}
