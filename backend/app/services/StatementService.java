package services;

import models.StatementModel;
import repositories.StatementRepository;
import repositories.GameRepository;

public class StatementService {
	private StatementRepository _claimRepo;
	
	
	public StatementService() {
		_claimRepo = new StatementRepository();
	}
	
	public StatementModel getUnusedRandomStatement(String gameName, String contentType) throws Exception{
		final int numRetries = 5;
		int tryNum = 0;
		
		StatementModel statement = new StatementModel(-1, null);
		while (tryNum <= numRetries) {
			statement = _claimRepo.getRandomStatement(contentType);
			boolean alreadyUsed = _claimRepo.isStatementAlreadyUsed(gameName, statement.id);
			
			if (!alreadyUsed) {
				_claimRepo.addUsedStatement(gameName, statement.id);
				return statement;
			}
			
			tryNum++;
		}
		
		return statement;
		
	}
	
	public void prepareUnusedRandomStatement(String gameName, String contentType, GameRepository gameRepo) throws Exception{
		
		StatementModel statement = getUnusedRandomStatement(gameName, contentType);
		gameRepo.updateCurrentStatement(gameName, statement.id);
		
	}
	
}
