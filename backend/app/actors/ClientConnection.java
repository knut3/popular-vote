package actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.FunctionRef;
import akka.actor.Props;
import akka.actor.Scheduler;
import akka.actor.Terminated;
import models.ClientMessage;
import play.libs.EventSource;
import play.libs.EventSource.Event;
import play.libs.Json;

public class ClientConnection extends AbstractActor {

	private ActorRef _eventSourceActor;
	
	public ClientConnection(String userId, ActorRef eventSourceActor) {
		
		_eventSourceActor = eventSourceActor;
		
		getContext().watch(_eventSourceActor);
		
	}
	
	public static Props getProps(String connectionId, ActorRef eventSourceActor) {
        return Props.create(ClientConnection.class, () -> new ClientConnection(connectionId, eventSourceActor));
    }
	
	@Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(
                Terminated.class,
                terminated -> {
                    if (terminated.getActor().equals(_eventSourceActor)) {
                        getContext().stop(self());
                    }
                }
            )
            .match(
                ClientMessage.class,
                clientMessage -> {
                	Event event = EventSource.Event.event(Json.toJson(clientMessage.data));
                	event = event.withName(clientMessage.event);
                	event = event.withId(clientMessage.id.toString());
                    _eventSourceActor.tell(event, getSelf());
                }
            )
            .build();
    }

}
