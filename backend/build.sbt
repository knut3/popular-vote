name := """backend"""
organization := "info.knuts"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.2"

libraryDependencies += guice

libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.19"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor"  % "2.6.5",
  "com.typesafe.akka" %% "akka-slf4j"  % "2.6.5",
  "com.typesafe.akka" %% "akka-actor-typed"  % "2.6.5",
  "com.typesafe.akka" %% "akka-protobuf-v3"  % "2.6.5",
  "com.typesafe.akka" %% "akka-stream"  % "2.6.5"
)

EclipseKeys.preTasks := Seq(compile in Compile, compile in Test)

