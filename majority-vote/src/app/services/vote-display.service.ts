import { Injectable } from '@angular/core';
import { ActionSheetController, AlertController } from '@ionic/angular';
import { Vote } from 'src/app/models/vote.model';
import { Storage } from '@ionic/storage';
import { VoteService } from 'src/app/services/vote.service';
import { StorageKeys } from '../misc/storage-keys';
import { OverlayBaseController } from '@ionic/angular/util/overlay';



@Injectable({
  providedIn: 'root'
})
export class VoteDisplayService {

  actionSheet:HTMLIonActionSheetElement;
  alert:HTMLIonAlertElement;
  hasVoted:boolean;

  constructor(private actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private storage : Storage,
    private voteService : VoteService) { }

  async updateVoteDisplay(gameName:string, vote:Vote, hasVoted:boolean){

    if (this.hasVoted == undefined){
      this.hasVoted = hasVoted;
    }

    if (this.actionSheet == undefined && this.alert == undefined && !this.hasVoted){
      this.presentActionSheet(gameName, vote);
    }
    else if (this.alert == undefined && this.hasVoted){
      this.presentAlert(vote);
    }
    else if (this.hasVoted){
      this.alert.message = this.getVoteResults(vote);
    }
    else{
      this.actionSheet.subHeader = this.getVoteResults(vote);
    }
  }

  reset(){
    if (this.actionSheet != undefined){
      this.actionSheet.dismiss();
      this.actionSheet = undefined;
    }

    if (this.alert != undefined){
      this.alert.dismiss();
      this.alert = undefined;
    }

    this.hasVoted = undefined;
  }

  private async presentActionSheet(gameName:string, vote:Vote) {
    this.actionSheet = await this.actionSheetController.create({
      header: "Skip vote",
      subHeader: this.getVoteResults(vote),
      backdropDismiss: false,
      buttons: [{
        text: 'Agree!',
        icon: 'checkmark',
        handler: () => {
          this.storage.get(StorageKeys.UserId).then(
            userId => {
              this.hasVoted = true;
              this.voteService.submitVote(gameName, userId, true).subscribe()
            }
          )
        }
      }, {
        text: 'Disagree!',
        icon: 'close',
        handler: () => {
          this.storage.get(StorageKeys.UserId).then(
            userId => {
              this.hasVoted = true
              this.voteService.submitVote(gameName, userId, false).subscribe()
            }
          )
        }
      }]
    });
    await this.actionSheet.present();
  }

  private async presentAlert(vote:Vote) {
    this.alert = await this.alertController.create({
      header: 'Skip vote',
      backdropDismiss: false,
      message: this.getVoteResults(vote),
    });

    await this.alert.present();
  }

  private getVoteResults(vote:Vote) : string {
    return `${vote.yesVotes} of ${vote.votesTotal} players want to skip thus far. ${vote.yesVotesNeeded} votes needed to skip.`
  }
}
