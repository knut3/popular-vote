import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServerInfo } from '../misc/server-info';
import { Vote } from '../models/vote.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VoteService {

  private readonly apiUrl = ServerInfo.Address + "/votes"

  constructor(private http : HttpClient) { }


  startNewVote(gameName:string, userId:string) : Observable<any> {
     return this.http.post(`${this.apiUrl}/start-new?gameName=${gameName}&userId=${userId}`, null);
  }

  submitVote(gameName:string, userId:string, votedYes:boolean) : Observable<any> {
    return this.http.post(`${this.apiUrl}/submit?gameName=${gameName}&userId=${userId}&votedYes=${votedYes}`, null);
 }
}
