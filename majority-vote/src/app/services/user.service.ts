import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly apiUrl = "http://192.168.1.25:9000/users"

  constructor(private http: HttpClient) { }

  getNewUserId() : Observable<string> {
    const options : Object = {responseType: 'text'};
    return this.http.get<string>(`${this.apiUrl}/new-id`, options );
  }

}
