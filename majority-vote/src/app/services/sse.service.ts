import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerSentEventModel } from '../models/server-sent-event.model';
import { Storage } from '@ionic/storage';
import { StorageKeys } from '../misc/storage-keys';

@Injectable({
  providedIn: 'root'
})
export class SseService {

  private readonly apiRootUrl = "http://192.168.1.25:9000";
  private eventSource : EventSource;

  constructor(private storage: Storage){

  }

  startGettingServerSentEvents(userId:string){  
      this.eventSource = new EventSource(this.apiRootUrl + "/events/listen?userId=" + userId);
  }

  getEventSource() : Promise<EventSource>{
    return new Promise((resolve, reject) => {
      if (this.eventSource == undefined){
        this.storage.get(StorageKeys.UserId).then(
          userId => {
            this.startGettingServerSentEvents(userId);
            resolve(this.eventSource);
          }
        )
      }
      else{
        resolve(this.eventSource);
      }
    });
    
  }

}
