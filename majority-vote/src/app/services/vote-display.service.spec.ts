import { TestBed } from '@angular/core/testing';

import { VoteDisplayService } from './vote-display.service';

describe('VoteDisplayService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VoteDisplayService = TestBed.get(VoteDisplayService);
    expect(service).toBeTruthy();
  });
});
