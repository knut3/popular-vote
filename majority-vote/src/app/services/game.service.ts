import { Injectable, NgZone } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { CreateGameModel } from '../models/create-game.model'
import { Availability } from '../models/availability.model';
import { Player } from '../models/player.model';
import { SseService } from './sse.service';
import { JoinGameModel } from '../models/join-game.model';
import { StatementModel } from '../models/statement.model';
import { ServerInfo } from '../misc/server-info';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private readonly apiUrl = ServerInfo.Address + "/games"

  constructor(private http : HttpClient) { }


  gameNameAvailability(gameName:string) : Observable<Availability> {
     return this.http.get<Availability>(`${this.apiUrl}/name-availability?gameName=${gameName}`);
  }

  playerNameAvailability(gameName:string, playerName:string) : Observable<Availability> {
    return this.http.get<Availability>(`${this.apiUrl}/player-name-availability?gameName=${gameName}&playerName=${playerName}`);
  }

  isCreator(gameName:string, userId:string) : Observable<Availability> {
    return this.http.get<Availability>(`${this.apiUrl}/is-creator?gameName=${gameName}&userId=${userId}`);
  }

  createGame(creatorId:string, creatorName:string, gameName:string, gameType:string) : Observable<Object>{

    var model = new CreateGameModel();
    model.creatorId = creatorId;
    model.creatorName = creatorName;
    model.gameName = gameName;
    model.gametype = gameType;
    return this.http.post(`${this.apiUrl}/create`, model);
  }

  joinGame(gameName:string, userId:string, username:string) : Observable<Object>{

    var model = new JoinGameModel();
    model.userId = userId;
    model.username = username;
    model.gameName = gameName;
    return this.http.post(`${this.apiUrl}/join`, model);
  }

  getPlayers(gameName:string) : Observable<Array<Player>> {
    return this.http.get<Array<Player>>(`${this.apiUrl}/players?gameName=${gameName}`);
  }

  prepareNewStatement(gameName:string){
   return this.http.put(`${this.apiUrl}/prepare-new-statement?gameName=${gameName}`, null);
  }

  startNewRound(gameName:string){
    return this.http.post(`${this.apiUrl}/start-new-round?gameName=${gameName}`, null);
   }

  getCurrentStatement(gameName:string) : Observable<StatementModel>{
    return this.http.get<StatementModel>(`${this.apiUrl}/current-statement?gameName=${gameName}`);
  }

  getCurrentStatementComment(gameName:string) : Observable<StatementModel>{
    return this.http.get<StatementModel>(`${this.apiUrl}/current-statement-comment?gameName=${gameName}`);
  }

}
