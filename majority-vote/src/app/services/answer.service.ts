import { Injectable } from '@angular/core';
import { ServerInfo } from '../misc/server-info';
import { HttpClient } from '@angular/common/http';
import { SubmitAnswerModel } from '../models/submit-answer.model';
import { Observable } from 'rxjs';
import { SubmitAnswerResponseModel } from '../models/submit-answer-response.model';
import { PlayerVotes } from '../models/player-votes.model';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  private readonly apiUrl : string = ServerInfo.Address + "/answers"

  constructor(private http : HttpClient) { }

  submitAnswer(gameName:string, submitterId:string, answerUserId:string) : Observable<SubmitAnswerResponseModel>{

    const model = new SubmitAnswerModel();
    model.gameName = gameName;
    model.submitterId = submitterId;
    model.answerUserId = answerUserId;

    return this.http.post<SubmitAnswerResponseModel>(`${this.apiUrl}/submit`, model);
  }

  getPlayerVotes(gameName:string) : Observable<PlayerVotes[]>{
    return this.http.get<PlayerVotes[]>(`${this.apiUrl}?gameName=${gameName}`);
  }
}
