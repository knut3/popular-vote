import { Injectable } from '@angular/core';
import { Player } from '../models/player.model';

@Injectable({
  providedIn: 'root'
})
export class GameStateService {

  gameName : string;
  player : Player;
  isCreator : boolean;

  constructor() { }

  setGameName(gameName: string){
    this.gameName = gameName;
  }

  getGameName() : string {
    return this.gameName;
  }

  setPlayer(id:string, name:string){
    this.player = new Player(id, name);
  }

  getPlayer() : Player {
    return this.player;
  }

  setIsCreator(isCreator : boolean){
    this.isCreator = isCreator;
  }

  getIsCreator() : boolean{
    return this.isCreator;
  }

}
