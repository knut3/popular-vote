import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/startmenu/startmenu.module').then( m => m.StartmenuPageModule)
  },
  {
    path: 'new-game',
    loadChildren: () => import('./pages/new-game/new-game.module').then( m => m.NewGamePageModule)
  },
  {
    path: 'join-game',
    loadChildren: () => import('./pages/join-game/join-game.module').then( m => m.JoinGamePageModule)
  },
  {
    path: 'lobby/:gameName',
    loadChildren: () => import('./pages/lobby/lobby.module').then( m => m.LobbyPageModule)
  },
  {
    path: 'statement/:gameName',
    loadChildren: () => import('./pages/statement/statement.module').then( m => m.StatementPageModule)
  },
  {
    path: 'answer-submitted/:gameName',
    loadChildren: () => import('./pages/answer-submitted/answer-submitted.module').then( m => m.AnswerSubmittedPageModule)
  },
  {
    path: 'round-result/:gameName',
    loadChildren: () => import('./pages/round-result/round-result.module').then( m => m.RoundResultPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
