export class EventNames{
    static PlayerJoined = "player-joined";
    static NewStatementReady = "new-statement-ready";
    static NewRoundStarting = "new-round-starting";
    static SkipVoteStarted = "skip-vote-started";
    static NewSkipVoteIn = "new-skip-vote-in";
    static VoteFinished = "vote-finished";
    static RoundCompleted = "round-completed";
}