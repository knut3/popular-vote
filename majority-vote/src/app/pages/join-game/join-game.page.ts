import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { combineLatest } from 'rxjs';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';
import { Router } from '@angular/router';

@Component({
  selector: 'app-join-game',
  templateUrl: './join-game.page.html',
  styleUrls: ['./join-game.page.scss'],
})
export class JoinGamePage implements OnInit {

  playerName = "";
  gameName = "";
  viewPlayerNameErrorMsg = false;
  viewGameNameEmptyErrorMsg = false;
  viewPlayerNameTakenErrorMsg = false;
  viewWrongGameNameErrorMsg = false;

  constructor(private service:GameService, private storage:Storage, private router:Router) { }

  ngOnInit() {
  }

  joinGame(){
    if (this.playerName == ""){
      this.viewPlayerNameErrorMsg = true;
    }
    else if (this.gameName == ""){
      this.viewGameNameEmptyErrorMsg = true;
      this.viewPlayerNameErrorMsg = false;
    }
    else{
      this.viewGameNameEmptyErrorMsg = false;
      this.viewPlayerNameErrorMsg = false;

      const playerNameAvailability$ = this.service.playerNameAvailability(this.gameName, this.playerName);
      const gameNameAvailability$ = this.service.gameNameAvailability(this.gameName);

      combineLatest(playerNameAvailability$, gameNameAvailability$).subscribe(
        ([playerNameAvailability, gameNameAvailability]) => {

          if (gameNameAvailability.isAvailable){
            this.viewWrongGameNameErrorMsg = true;
            this.viewPlayerNameTakenErrorMsg = false;
            return;
          }

          if (!playerNameAvailability.isAvailable){
            this.viewPlayerNameTakenErrorMsg = true;
            this.viewWrongGameNameErrorMsg = false;
            return;
          }

          this.storage.set(StorageKeys.Username, this.playerName);
          this.storage.get(StorageKeys.UserId).then(
            userId => this.service.joinGame(this.gameName, userId, this.playerName).subscribe(
              () => this.router.navigate(["lobby", this.gameName])
            )
          )

        }
      );

    }
  }

}
