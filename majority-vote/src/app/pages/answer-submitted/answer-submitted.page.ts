import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SseService } from 'src/app/services/sse.service';
import { EventNames } from 'src/app/misc/event-names';
import { GameService } from 'src/app/services/game.service';

@Component({
  selector: 'app-answer-submitted',
  templateUrl: './answer-submitted.page.html',
  styleUrls: ['./answer-submitted.page.scss'],
})
export class AnswerSubmittedPage {
  
  gameName:string;
  comment:string;

  constructor(private route : ActivatedRoute,
    private sseService:SseService,
    private router:Router,
    private gameService:GameService) { }

  ionViewWillEnter(){
    this.gameName = this.route.snapshot.params.gameName;

    this.gameService.getCurrentStatementComment(this.gameName).subscribe(
      model => this.comment = model.value
    );

    this.sseService.getEventSource().then(
      eventSource => {
        eventSource.addEventListener(EventNames.RoundCompleted,
          _ => this.router.navigate(["round-result", this.gameName])
        );
      }
    );
  }

}
