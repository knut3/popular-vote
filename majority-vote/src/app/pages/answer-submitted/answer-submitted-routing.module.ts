import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnswerSubmittedPage } from './answer-submitted.page';

const routes: Routes = [
  {
    path: '',
    component: AnswerSubmittedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnswerSubmittedPageRoutingModule {}
