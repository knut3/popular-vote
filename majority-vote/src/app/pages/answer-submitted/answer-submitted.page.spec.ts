import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AnswerSubmittedPage } from './answer-submitted.page';

describe('AnswerSubmittedPage', () => {
  let component: AnswerSubmittedPage;
  let fixture: ComponentFixture<AnswerSubmittedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnswerSubmittedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AnswerSubmittedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
