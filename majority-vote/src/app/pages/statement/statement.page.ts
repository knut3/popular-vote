import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StatementModel } from 'src/app/models/statement.model';
import { Player } from 'src/app/models/player.model';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';
import { VoteService } from 'src/app/services/vote.service';
import { SseService } from 'src/app/services/sse.service';
import { EventNames } from 'src/app/misc/event-names';
import { VoteDisplayService } from 'src/app/services/vote-display.service';
import { Vote } from 'src/app/models/vote.model';
import { VoteFinishedModel } from 'src/app/models/vote-finished.model';
import { ToastController } from '@ionic/angular';
import { AnswerService } from 'src/app/services/answer.service';


@Component({
  selector: 'app-statement',
  templateUrl: './statement.page.html',
  styleUrls: ['./statement.page.scss'],
})
export class StatementPage {

  gameName : string;
  statement : StatementModel = new StatementModel(null, "...");
  players : Player[];
  answer : string;
  hasVoted : boolean = false;

  constructor(private gameService : GameService,
    private route : ActivatedRoute,
    private storage : Storage,
    private voteService : VoteService,
    private sseService : SseService,
    private voteDisplayService:VoteDisplayService,
    private toastController:ToastController,
    private answerService:AnswerService,
    private router:Router
    ) { }

  ionViewWillEnter(){
    this.gameName = this.route.snapshot.params.gameName;
    this.hasVoted = false;
    this.answer = undefined;

    this.gameService.getCurrentStatement(this.gameName).subscribe(
      statement => this.statement = statement 
    )

    this.gameService.getPlayers(this.gameName).subscribe(
      players => this.players = players
    );

    this.sseService.getEventSource().then(
      eventSource => {
        eventSource.addEventListener(EventNames.SkipVoteStarted, 
          (event : MessageEvent) => {
              this.voteDisplayService.updateVoteDisplay(this.gameName, JSON.parse(event.data), this.hasVoted)
          }
        );

        eventSource.addEventListener(EventNames.NewSkipVoteIn,
          (event : MessageEvent) => {
            this.voteDisplayService.updateVoteDisplay(this.gameName, JSON.parse(event.data), null)
          }
        );

        eventSource.addEventListener(EventNames.VoteFinished,
          (event : MessageEvent) => {
            const result = JSON.parse(event.data) as VoteFinishedModel

            this.voteDisplayService.reset();
            this.hasVoted = false;

            if (result.votePassed){
              this.gameService.getCurrentStatement(this.gameName).subscribe(
                statement => {
                  this.statement = statement;
                }
              );
              this.presentToast("Vote Passed", 2000); 
            }
            else{
              this.presentToast("Vote failed", 2000);
            }
          }
        );
      }
    );

  }

  startSkipVote(){
    this.storage.get(StorageKeys.UserId).then(
      userId => {
        this.hasVoted = true;
        this.voteService.startNewVote(this.gameName, userId).subscribe();
      }
    )
  }

  submit(){
    this.storage.get(StorageKeys.UserId).then(
      userId => this.answerService.submitAnswer(this.gameName, userId, this.answer).subscribe(
        model => {
          if (model.lastToHandIn){
            this.router.navigate(["round-result", this.gameName])
          }
          else{
            this.router.navigate(["answer-submitted", this.gameName])      
          }
        }
      )
    )
  }

  private async presentToast(message:string, duration:number) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration
    });
    toast.present();
  }

  

}
