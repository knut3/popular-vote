import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartmenuPage } from './startmenu.page';

const routes: Routes = [
  {
    path: '',
    component: StartmenuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartmenuPageRoutingModule {}
