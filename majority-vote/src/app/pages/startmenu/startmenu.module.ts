import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartmenuPageRoutingModule } from './startmenu-routing.module';

import { StartmenuPage } from './startmenu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartmenuPageRoutingModule
  ],
  declarations: [StartmenuPage]
})
export class StartmenuPageModule {}
