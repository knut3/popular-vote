import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StartmenuPage } from './startmenu.page';

describe('StartmenuPage', () => {
  let component: StartmenuPage;
  let fixture: ComponentFixture<StartmenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartmenuPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StartmenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
