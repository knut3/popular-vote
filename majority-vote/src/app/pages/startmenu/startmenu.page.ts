import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';

@Component({
  selector: 'app-startmenu',
  templateUrl: './startmenu.page.html',
  styleUrls: ['./startmenu.page.scss'],
})
export class StartmenuPage implements OnInit {

  constructor(
    private storage: Storage,
    private userService: UserService
    ) { }

  
  ngOnInit() {
    this.storage.get(StorageKeys.UserId).then(userId => { if(userId == null) this.setNewUserId() } );
  }

  setNewUserId(){
    this.userService.getNewUserId().subscribe(userId => this.storage.set(StorageKeys.UserId, userId));
  }

}
