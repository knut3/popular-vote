import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoundResultPage } from './round-result.page';

describe('RoundResultPage', () => {
  let component: RoundResultPage;
  let fixture: ComponentFixture<RoundResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoundResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
