import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoundResultPageRoutingModule } from './round-result-routing.module';

import { RoundResultPage } from './round-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoundResultPageRoutingModule
  ],
  declarations: [RoundResultPage]
})
export class RoundResultPageModule {}
