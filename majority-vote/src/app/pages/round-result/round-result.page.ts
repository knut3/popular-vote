import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AnswerService } from 'src/app/services/answer.service';
import { PlayerVotes } from 'src/app/models/player-votes.model';
import { GameService } from 'src/app/services/game.service';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';
import { SseService } from 'src/app/services/sse.service';
import { EventNames } from 'src/app/misc/event-names';

@Component({
  selector: 'app-round-result',
  templateUrl: './round-result.page.html',
  styleUrls: ['./round-result.page.scss'],
})
export class RoundResultPage{

  gameName:string;
  playersWithVotes:PlayerVotes[];
  isCreator:boolean;

  constructor(private route : ActivatedRoute,
    private answerService:AnswerService,
    private gameService:GameService,
    private storage:Storage,
    private sseService:SseService,
    private router:Router) { }

  ionViewWillEnter(){
    this.gameName = this.route.snapshot.params.gameName;

    this.answerService.getPlayerVotes(this.gameName).subscribe(
      model => this.playersWithVotes = model
    );

    this.storage.get(StorageKeys.UserId).then(
      userId => this.gameService.isCreator(this.gameName, userId).subscribe(
        res => this.isCreator = res.isAvailable
      )
    );

    this.sseService.getEventSource().then(
      eventSource => {

        eventSource.addEventListener(EventNames.NewRoundStarting,
          _ => this.router.navigate(["statement", this.gameName])
        );
      }
    );
    
  }

  startNextRound(){
    this.gameService.startNewRound(this.gameName).subscribe();
  }

}
