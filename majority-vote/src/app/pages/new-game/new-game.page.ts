import { Component, OnInit } from '@angular/core';
import { GameService } from 'src/app/services/game.service';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';
import { GameStateService } from 'src/app/services/game-state.service';
import { Player } from 'src/app/models/player.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.page.html',
  styleUrls: ['./new-game.page.scss'],
})
export class NewGamePage implements OnInit {

  playerName = "";
  gameName = "";
  contentType = "no-explicit";

  viewPlayerNameErrorMsg = false;
  viewGameNameEmptyErrorMsg = false;
  viewGameNameTakenErrorMsg = false;

  constructor(
    private gameService : GameService, 
    private storage : Storage, 
    private router : Router) { 
  }

  ngOnInit() {
    
  }

  gameTypeChanged(event){
    this.contentType = event.target.value;
  }

  checkGameNameAvailability(){
    this.gameService.gameNameAvailability(this.gameName).subscribe(
      res => res.isAvailable ? this.viewGameNameTakenErrorMsg = false : this.viewGameNameTakenErrorMsg = true
    );
  }

  private sendCreateGameRequest(){
    this.storage.get(StorageKeys.UserId).then(userId =>
      this.gameService.createGame(userId, this.playerName, this.gameName, this.contentType).subscribe(
        () => this.gotoLobby(userId)
      )
    );
    this.storage.set(StorageKeys.Username, this.playerName);
  }

  private gotoLobby(userId : string){
    this.router.navigate(["lobby", this.gameName]);
  }

  startGame(){
    if (this.playerName == ""){
        this.viewPlayerNameErrorMsg = true;
    }
    else if (this.gameName == ""){
      this.viewGameNameEmptyErrorMsg = true;
    }
    else{
        
      this.gameService.gameNameAvailability(this.gameName).subscribe(
        res => res.isAvailable ? this.sendCreateGameRequest() : this.viewGameNameTakenErrorMsg = true
      );
      
      
      
    }

  }

}
