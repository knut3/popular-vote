import { Component, OnInit } from '@angular/core';
import { Player } from 'src/app/models/player.model';
import { GameStateService } from 'src/app/services/game-state.service';
import { GameService } from 'src/app/services/game.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { StorageKeys } from 'src/app/misc/storage-keys';
import { SseService } from 'src/app/services/sse.service';
import { EventNames } from 'src/app/misc/event-names';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.page.html',
  styleUrls: ['./lobby.page.scss'],
})
export class LobbyPage implements OnInit {

  gameName : string;
  isCreator: boolean;
  players: Array<Player> = [];


  constructor(private gameState: GameStateService,
    private gameService : GameService,
    private sseService : SseService,
    private route: ActivatedRoute,
    private storage: Storage,
    private router : Router) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.gameName = this.route.snapshot.params.gameName;
    const players$ = this.gameService.getPlayers(this.gameName).toPromise();

    const userId$ = this.storage.get(StorageKeys.UserId);
    
    Promise.all([players$, userId$]).then(
      data => {
        const users = data[0];
        const userId = data[1];

        this.players = users;

        this.gameService.isCreator(this.gameName, userId).subscribe(
          res => this.isCreator = res.isAvailable
          );

        this.sseService.startGettingServerSentEvents(userId);

        this.sseService.getEventSource().then(
          eventSource => {
            eventSource.addEventListener(EventNames.PlayerJoined, 
              (event : MessageEvent) => {
                this.players.push(JSON.parse(event.data));
              }
            );

            eventSource.addEventListener(EventNames.NewRoundStarting,
              event => this.router.navigate(["statement", this.gameName])
            );
          }
        );
      }
    );
  }

  startGame(){
    this.gameService.startNewRound(this.gameName).subscribe();
  }

}
