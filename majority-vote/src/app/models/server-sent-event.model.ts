export class ServerSentEventModel {

    id : String;
    event : String;
    data : Object;

}