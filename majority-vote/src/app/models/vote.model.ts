export class Vote {

    votesTotal : number;
    yesVotes : Number;
    yesVotesNeeded : number;

    constructor(votesTotal:number, yesVotes:number, yesVotesNeeded:number){
        this.votesTotal = votesTotal;
        this.yesVotes = yesVotes;
        this.yesVotesNeeded = yesVotesNeeded;
    }

}