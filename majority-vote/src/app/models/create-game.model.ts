export class CreateGameModel{
    creatorId : string;
    creatorName : string;
    gameName : string;
    gametype : string;
}